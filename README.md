#The Movies And Shows You Love Are Destroying Your Emotions.

###*“The movies make emotions look so strong and real, whereas when things really do happen to you, it’s like watching tele-vision—you don’t feel anything”* 
*(Steven Shaviro)*

You hear this all the time when listening to reasons behind why people want to watch a movie/show.

-Horror movies give us *adrenaline* from being *scared*

-Romantic movies make us feel *loving*

-Comedies make us *happy* and *laugh*

####They make us *Feel*

https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=imgres&cd=&cad=rja&uact=8&ved=0ahUKEwit2peE_63WAhVGxoMKHao-BMwQjRwIBw&url=https%3A%2F%2Fonsizzle.com%2Ft%2Fglass-case-of-emotions%3Fs%3Dnew&psig=AFQjCNHmPZUBC08a4iPQqdShfV5RnOeryg&ust=1505798594917964

With this however, science shows that this has actually been *dampening* our emotional responses to things that happen in real life.  For example; if you were to watch horror movies daily, adrenaline levels would spike, *initially*, but would go down the more you watch over time.

This isn't just movies/shows though.

Growing up near Chicago, whenever I watched any 
News 
channel at night they'd talk about the crimes that have happened today or during the week.  Hearing about all the tragic things going on used to really upset me when I was young.  What I realized though, is that over time I started to not really *care*.  This was because it was what I was *used* to.

What happens to our brains is that whatever it is we're watching, we become *adapted* to anything that release hormones in our bodies.